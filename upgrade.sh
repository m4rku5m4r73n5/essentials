#!/usr/bin/env bash
set -e
#
# source <(wget --no-cache -qO- https://bitbucket.org/m4rku5m4r73n5/essentials/raw/master/upgrade.sh)
#
sudo apt-get update -y
sudo apt-get upgrade -y
sudo apt-get autoremove -y
