#!/usr/bin/env bash
set -e
#
# source <(wget --no-cache -qO- https://bitbucket.org/m4rku5m4r73n5/essentials/raw/master/loffice.sh)
#
cd ~
tar -cvzf ~/Dropbox/Settings/loffice.tar.gz .config/libreoffice/*/user/config/soffice.cfg/modules/*/toolbar/custom_toolbar_*.xml
