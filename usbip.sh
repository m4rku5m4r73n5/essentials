#!/usr/bin/env bash
set -e
# https://wiki.ubuntuusers.de/USBIP/
#
# SERVER (Raspbian Jessy)
#
# enable required modules
modprobe usbip_core
modprobe usbip_host
#
# list available devices to share
usbip list -l
# - busid 1-1.1 (0424:ec00)
#   Standard Microsystems Corp. : SMSC9512/9514 Fast Ethernet Adapter (0424:ec00)
#
# - busid 1-1.2 (0bda:8172)
#   Realtek Semiconductor Corp. : RTL8191SU 802.11n WLAN Adapter (0bda:8172)
#
# - busid 1-1.3.1 (0403:8a8a)
#   Future Technology Devices International, Ltd : unknown product (0403:8a8a)
#
usbip -d bind -b 1-1.3.1
# usbip: info: bind device on busid 1-1.3.1: complete
#
# CLIENT (Ubuntu 16.04)
#
# do not use outdated "apt-get install usbip" [!!!]
#
# install usbip (core since 3.17)
sudo apt-get install linux-tools-generic
#
sudo modprobe vhci-hcd
#
# list remote shares
/usr/bin/usbip list -r 192.168.178.10
# Exportable USB devices
# ======================
#  - 192.168.178.10
#     1-1.3.1: Future Technology Devices International, Ltd : unknown product (0403:8a8a)
#            : /sys/devices/platform/soc/20980000.usb/usb1/1-1/1-1.3/1-1.3.1
#            : (Defined at Interface level) (00/00/00)
#
/usr/bin/usbip attach -r 192.168.178.10 -b 1-1.3.1
#
lsusb
# Bus 005 Device 003: ID 0403:8a8a Future Technology Devices International, Ltd
# ...
