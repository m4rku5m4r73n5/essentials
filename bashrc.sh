#!/usr/bin/env bash
set -e
#
PS1="\e[44;33m\t \e[47;31m \u@\h \e[45;30m $(git rev-parse --abbrev-ref HEAD 2>/dev/null || echo [NOGIT]) \e[42;30m \w \e[0m\n > "
EDITOR="mcedit"
#
alias ..='cd ..'
alias dir='ls --color -lah'
alias cls='tput clear'
alias -- ~='cd ~'
alias -- -="cd -"
alias -- ?="git status"
