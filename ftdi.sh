#!/usr/bin/env bash
set -e
#
# source <(wget --no-cache -qO- https://bitbucket.org/m4rku5m4r73n5/essentials/raw/master/ftdi.sh)
#
set -e
#
if [[ $EUID -ne 0 ]]; then echo "This script must be run as root" 1>&2 ; return ; fi
#
/sbin/modprobe ftdi_sio
echo "0403 8a8a" > /sys/bus/usb-serial/drivers/ftdi_sio/new_id
#
# TODO: https://wiki.siduction.de/index.php?title=Symlink_zur_eindeutigen_Erkennung_mittels_udev-Regel
#

