#!/usr/bin/env bash
set -e
#
# hide mounts from launcher
#
# http://www.liberiangeek.net/2012/03/show-hide-mounted-volumes-on-the-desktop-in-ubuntu-11-10-oneiric-ocelot/
gsettings set org.gnome.nautilus.desktop volumes-visible true
#
# http://askubuntu.com/questions/159029/how-to-hide-mounted-partitions-and-devices-from-unity-launcher-via-command-line
gconftool --set /apps/compiz-1/plugins/unityshell/screen0/options/devices_option --type=int 0
#
# install taskbar cpu/memory monitor
sudo apt-get install indicator-multiload

