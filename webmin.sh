#!/usr/bin/env bash
set -e
#
# source <(wget --no-cache -qO- https://bitbucket.org/m4rku5m4r73n5/essentials/raw/master/webmin.sh)
#
cd /tmp
#
echo -e "\e[32mInstall dependencies\e[0m"
sudo apt-get -y install perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python
#
echo -e "\e[32mDownload webmin package\e[0m"
wget http://www.webmin.com/download/deb/webmin-current.deb
#
echo -e "\e[32mInstall webmin\e[0m"
sudo dpkg --install webmin-current.deb
