#!/usr/bin/env bash
set -e
#
# source <(wget --no-cache -qO- https://bitbucket.org/m4rku5m4r73n5/essentials/raw/master/basics.sh)
#
if [[ $EUID -ne 0 ]]; then echo "must be root" 1>&2 ; fi
#
apt-get -y install sudo mc htop
