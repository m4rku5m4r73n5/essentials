#!/usr/bin/env bash
set -e
#
# source <(wget --no-cache -qO- https://bitbucket.org/m4rku5m4r73n5/essentials/raw/master/git.sh)
#
echo "Install packages:"
sudo apt-get install -y git git-flow
#
read -p "Enter mail: " INPUT
git config --global user.email $INPUT
read -p "Enter full name: " INPUT
git config --global user.name $INPUT
git config --global push.default simple
#
if [ ! -f ~/.ssh/id_rsa.pub ] ; then
    echo "Generate keys"
    ssh-keygen
fi
#
echo "Add public key to your git-hoster:"
cat ~/.ssh/id_rsa.pub
