#!/usr/bin/env bash
set -e
#
# source <(wget --no-cache -qO- https://bitbucket.org/m4rku5m4r73n5/essentials/raw/master/mm.sh)
#
echo -ne "apply personal settings..."
#
PS1="\e[44;33m\t \e[47;31m \u@\h \e[45;30m $(git rev-parse --abbrev-ref HEAD 2>/dev/null || echo [NOGIT]) \e[42;30m \w \e[0m\n > "
export PS1
#
if [ $(mcedit -V 1>&- 2>&- ; echo $?) == 0 ] ; then
    export EDITOR="mcedit"
fi
#
alias ..='cd ..'
alias -- ~='cd ~'
alias -- -="cd -"
alias -- ?="git status"
alias dc="docker-compose"
#
set +e
#
echo "done"
