#!/usr/bin/env bash
set -e
#
# source <(wget --no-cache -qO- https://bitbucket.org/m4rku5m4r73n5/essentials/raw/master/blueaudio.sh)
#
set -e
#
# Autoconnect Bluetooth headset with pulseaudio
# @see: https://help.ubuntu.com/community/BluetoothAudio
# @see: http://askubuntu.com/questions/141947/headset-input-setting-as-default
echo "load-module module-switch-on-connect" >> /etc/pulse/default.pa
