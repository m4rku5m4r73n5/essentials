#!/usr/bin/env bash
set -e
#
# source <(wget --no-cache -qO- https://bitbucket.org/m4rku5m4r73n5/essentials/raw/master/raspberry.sh)
#
if [[ "$USER" = "pi" ]] ; then
    echo "Please login as root and run this script again"
    sudo su
elif [[ $EUID -eq 0 ]] ; then
    echo "Please..."
    echo "- change password (!)"
    echo "- enable SSH"
    echo "- enable serial hardware"
    echo "- disable serial login"
    read -n1 -r -p "Press any key to continue..." INPUT
    raspi-config
    wget --no-cache -qO- https://bitbucket.org/m4rku5m4r73n5/essentials/raw/master/bashrc.sh >> /etc/skel/.bashrc
    #
    read -p "new user:" USERNAME
    if [[ -z "$USERNAME" ]] ; then echo "username required" ; return ; fi
    groupmod --new-name $USERNAME pi
    usermod --move-home --login $USERNAME --home /home/$USERNAME pi
    usermod -a -G pi,dialout $USERNAME
    echo "$USERNAME ALL = NOPASSWD: ALL" > /etc/sudoers.d/$USERNAME
    chmod 0440 /etc/sudoers.d/$USERNAME
    id $USERNAME
    #
    echo "install packages:"
    source <(wget --no-cache -qO- https://bitbucket.org/m4rku5m4r73n5/essentials/raw/master/basics.sh)
    apt-get install -y nfs-common nfs-kernel-server git git-flow python-pip screen
    #
    echo "configure NFS"
    echo "/home/$USERNAME 192.168.178.0/255(rw,nohide,no_subtree_check,sync)" >> /etc/exports
    #
    echo "Please login as $USERNAME after reboot and run this script again"
    reboot
else
    sudo userdel -r pi
    sudo rm /etc/sudoers.d/010_pi-nopasswd
    if [[ ! -f ~/.ssh/id_rsa.pub ]] ; then ssh-keygen ; fi
    echo "add following key to https://bitbucket.org/"
    cat ~/.ssh/id_rsa.pub
    # enable ttyUSB access
fi
