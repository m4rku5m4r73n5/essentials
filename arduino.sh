#!/usr/bin/env bash
set -e
#
# source <(wget --no-cache -qO- https://bitbucket.org/m4rku5m4r73n5/essentials/raw/master/arduino.sh)
#
if [[ $EUID -ne 0 ]]; then echo "This script must be run as root" 1>&2 ; return ; fi
#
apt-get install -y git python-serial
#
cd /opt
#
wget https://downloads.arduino.cc/arduino-1.8.3-linuxarm.tar.xz
#
tar xvf arduino-1.8.3-linuxarm.tar.xz
#
rm arduino-1.8.3-linuxarm.tar.xz
#
ln -s arduino-1.8.3 arduino
#
git clone https://github.com/sudar/Arduino-Makefile.git
#
cat << EOF > Arduino-Makefile/.bashrc
export ARDUINO_DIR=/opt/arduino
export ARDMK_DIR=/opt/Arduino-Makefile
export AVR_TOOLS_DIR=/opt/arduino/hardware/tools/avr
EOF
#
echo "source /opt/Arduino-Makefile/.bashrc" >> /etc/bash.bashrc
